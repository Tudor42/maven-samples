Maven samples
==================

## Sample multi-module maven project integration with GitLab and Codacy

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/155f0a0a5d774f0485e0db7434897a95)](https://app.codacy.com/gl/Tudor42/maven-samples/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/155f0a0a5d774f0485e0db7434897a95)](https://app.codacy.com/gl/Tudor42/maven-samples/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_coverage)