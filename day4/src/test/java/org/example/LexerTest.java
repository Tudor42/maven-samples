package org.example;

import org.example.exceptions.InvalidToken;
import org.example.tokens.*;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;

public class LexerTest {

    @Test
    public void tokenize_NormalFlow() throws Exception {
        String input = "2 + 3";

        List<Object> res = new ArrayList<>();
        res.add(new BigDecimal(2));
        res.add(InplaceOperation.ADD);
        res.add(new BigDecimal(3));
        assertThat(Lexer.tokenize(input), is(res));

        input = "-120 + 40";
        res = new ArrayList<>();
        res.add(InplaceOperation.SUBTRACT);
        res.add(new BigDecimal(120));
        res.add(InplaceOperation.ADD);
        res.add(new BigDecimal(40));
        assertThat(Lexer.tokenize(input), is(res));
    }

    @Test
    public void tokenize_InvalidToken() {
        assertThrows(InvalidToken.class, () -> Lexer.tokenize("&oda"));
        assertThrows(InvalidToken.class, () -> Lexer.tokenize("'12"));
        assertThrows(InvalidToken.class, () -> Lexer.tokenize("?"));
    }

}
