package org.example;

import org.example.exceptions.InvalidExpression;
import org.example.tokens.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThrows;

public class ParserTest {
    Parser evaluator;

    @Before
    public void setup() {
        evaluator = new Parser();
    }

    @Test
    public void parse_NormalFlow() throws InvalidExpression {
        assertEquals(evaluator.parse(
                List.of(new BigDecimal(12),
                        InplaceOperation.ADD,
                        new BigDecimal(-12))
        ), new BigDecimal(0));

        assertEquals(evaluator.parse(
                List.of(
                        new BigDecimal(1),
                        InplaceOperation.SUBTRACT,
                        new BigDecimal(20),
                        InplaceOperation.MULTIPLY,
                        new BigDecimal(-4))
        ), new BigDecimal(81));

        assertEquals(evaluator.parse(
                List.of(
                        SpecialToken.LPARENTHESIS,
                        new BigDecimal("12.2"),
                        InplaceOperation.ADD,
                        new BigDecimal("4"),
                        SpecialToken.RPARENTHESIS,
                        InplaceOperation.MULTIPLY,
                        new BigDecimal(4))
        ), new BigDecimal("64.8"));
    }

    @Test
    public void parse_MissingOperand() {
        assertThrows(InvalidExpression.class, () -> evaluator.parse(
                List.of(InplaceOperation.MULTIPLY,
                        new BigDecimal(12))
        ));
    }

    @Test
    public void parse_MissingOperator() {
        assertThrows(InvalidExpression.class, () -> evaluator.parse(
                List.of(new BigDecimal(12),
                        new BigDecimal(12))
        ));
    }

    @Test
    public void parse_MissingParenthesis() {
        assertThrows(InvalidExpression.class, () -> evaluator.parse(
                List.of(SpecialToken.RPARENTHESIS)
        ));
    }

    @Test
    public void parse_DivisionByZero() {
        assertThrows(ArithmeticException.class, () -> evaluator.parse(
                List.of(new BigDecimal(1),
                        InplaceOperation.DIVIDE,
                        new BigDecimal(0))
        ));
    }


}
