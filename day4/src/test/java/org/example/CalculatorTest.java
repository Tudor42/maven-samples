package org.example;

import org.example.exceptions.InvalidExpression;
import org.example.exceptions.InvalidToken;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class CalculatorTest {
    Parser parser;

    @Before
    public void setup(){
        parser = new Parser();
    }

    @Test
    public void integration_Expressions() throws InvalidToken, InvalidExpression {
        assertEquals(parser.parse(Lexer.tokenize("3^2+3*(11-1)")).setScale(0, RoundingMode.DOWN),
                new BigDecimal("39"));
        assertEquals(parser.parse(Lexer.tokenize("2*4+0.1")), new BigDecimal("8.1"));
        assertEquals(parser.parse(Lexer.tokenize("(2+1) % 4")), new BigDecimal("3"));
        assertEquals(parser.parse(Lexer.tokenize("4^0.5+min(max(1,2),4)")).intValue(), 4);
        assertEquals(parser.parse(Lexer.tokenize("4+2*2")), new BigDecimal(8));
        assertEquals(parser.parse(Lexer.tokenize("round(exp(1))")), new BigDecimal(3));
        assertEquals(parser.parse(Lexer.tokenize("log(exp(10))")).intValue(), 10);
        assertEquals(parser.parse(Lexer.tokenize("sqrt(4)")).intValue(), 2);
    }

    @Test
    public void integration_InvalidException() {
        assertThrows(InvalidExpression.class, () -> parser.parse(Lexer.tokenize("(")));
        assertThrows(InvalidExpression.class, () -> parser.parse(Lexer.tokenize(")")));
        assertThrows(InvalidExpression.class, () -> parser.parse(Lexer.tokenize("log 10")));
        assertThrows(InvalidExpression.class, () -> parser.parse(Lexer.tokenize("log(10")));
    }

    @Test
    public void integration_ArithmeticException() {
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("1/0")));
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("12 % 1.1")));
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("(0-12.3)^4.2")));
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("log(0-2)")));
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("round(1, 2)")));
        assertThrows(ArithmeticException.class, () -> parser.parse(Lexer.tokenize("sqrt(0-2)")));
    }
}
