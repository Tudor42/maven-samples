package org.example;

import org.example.exceptions.InvalidExpression;
import org.example.tokens.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Parser {

    private static final Logger logger = LoggerFactory.getLogger(Parser.class);

    private ListIterator<Object> tokenIt;
    private Object currToken;

    public BigDecimal parse(List<Object> tokens) throws InvalidExpression {
        logger.info("Parser parse {} tokens", tokens.size());
        tokenIt = tokens.listIterator();
        eatToken(); // move to first token
        BigDecimal res = parseExpression(5);
        if (currToken != SpecialToken.END_TOKEN) {
            throw new InvalidExpression("Expression cant be evaluated");
        }
        logger.info("Expression evaluated to {}", res);
        return res;
    }

    private void eatToken() {
        if (tokenIt.hasNext()) {
            currToken = tokenIt.next();
            logger.debug("Current token {}", currToken);
            return;
        }
        currToken = SpecialToken.END_TOKEN;
        logger.debug("All tokens eaten");
    }

    private BigDecimal parseExpression(int level) throws InvalidExpression {
        if (level <= 0) {
            return parseTerm();
        }
        logger.debug("Expression entry level {}", level);
        BigDecimal res = parseExpression(level - 1);
        InplaceOperation operation;
        while (currToken instanceof InplaceOperation &&
                (operation = (InplaceOperation) currToken).getPrecedence() == level) {
            eatToken();
            res = operation.calc(List.of(res, parseExpression(level - 1)));
        }
        logger.debug("Expression exit level {} with result {}", level, res);
        return res;
    }

    private BigDecimal parseTerm() throws InvalidExpression {
        logger.debug("Term parse");
        if (currToken instanceof BigDecimal) {
            BigDecimal res = (BigDecimal) currToken;
            eatToken();
            logger.debug("Term number {}", res);
            return res;
        }

        if (currToken instanceof FunctionOperation) {
            logger.debug("Term function");
            return parseFunction();
        }

        if (currToken == SpecialToken.LPARENTHESIS) {
            eatToken();
            logger.debug("Term expression");
            BigDecimal tmp = parseExpression(5);
            if (currToken != SpecialToken.RPARENTHESIS) {
                throw new InvalidExpression("Missing RPARENTHESIS");
            }
            eatToken();
            return tmp;
        }
        throw new InvalidExpression("Invalid tokens");
    }

    private BigDecimal parseFunction() throws InvalidExpression {
        FunctionOperation op = (FunctionOperation) currToken;
        eatToken();
        if (currToken != SpecialToken.LPARENTHESIS) {
            throw new InvalidExpression("Missing LPARENTHESIS for " + op);
        }
        eatToken();
        List<BigDecimal> listArgs = new ArrayList<>();
        listArgs.add(parseExpression(5));

        while (currToken == SpecialToken.COMMA) {
            eatToken();
            listArgs.add(parseExpression(5));
        }
        logger.debug("Evaluate function {} with arguments {}", op, listArgs);
        if (currToken != SpecialToken.RPARENTHESIS) {
            throw new InvalidExpression("Missing RPARENTHESIS for " + op);
        }
        eatToken();
        return op.calc(listArgs);
    }
}
