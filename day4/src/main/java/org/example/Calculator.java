package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Calculator {
    private static final Logger logger = LoggerFactory.getLogger(Calculator.class);

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String expr;
        Parser parse = new Parser();
        while (true) {
            System.out.print("$ ");
            expr = in.nextLine().trim();
            if (expr.isEmpty()) {
                continue;
            }
            if ("q".equals(expr)) {
                break;
            }
            try {
                System.out.println(parse.parse(Lexer.tokenize(makeZeroMinus(expr))));
            } catch (Exception e) {
                logger.error("Exception", e);
            }
        }
    }


    private static String makeZeroMinus(String expr) {
        Matcher matcher = Pattern.compile("(\\( *-)|(^ *-)|(, *-)").matcher(expr);
        return matcher.replaceAll(
                matchResult ->
                {
                    String s = matchResult.group();
                    return s.substring(0, s.length() - 1) + "0-";
                });
    }
}
