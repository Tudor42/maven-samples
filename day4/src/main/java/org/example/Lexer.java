package org.example;

import org.example.exceptions.InvalidToken;
import org.example.tokens.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

final public class Lexer {
    private static final Logger logger = LoggerFactory.getLogger(Lexer.class);

    private Lexer() {
    }

    public static List<Object> tokenize(String expression) throws InvalidToken {
        List<Object> tokens = new ArrayList<>();
        String trimmedExpression = expression.trim();
        logger.info("Lexer entry tokenize expression {}", trimmedExpression);
        Matcher matcher = Pattern.compile("").matcher(trimmedExpression);
        int startRegion = 0;
        while (!matcher.hitEnd()) {
            matcher.region(startRegion, trimmedExpression.length());
            Optional<?> opFound = Stream.of(SpecialToken.values(),
                            InplaceOperation.values(), FunctionOperation.values()).flatMap(Arrays::stream)
                    .filter(op->!Objects.isNull(op.toString()))
                    .filter(op -> {
                        matcher.usePattern(Pattern.compile("^" + op.toString() + " *"));
                        return matcher.find();
                    })
                    .findFirst();
            if (opFound.isPresent()) {
                logger.info("token of type {} found", ((Enum<?>)opFound.get()).name());
                if(opFound.get() == SpecialToken.NUMBER){
                    tokens.add(new BigDecimal(matcher.group().trim()));
                }else{
                    tokens.add(opFound.get());
                }
                startRegion = matcher.end();
                continue;
            }
            throw new InvalidToken("token unknown");
        }
        logger.info("Lexer exit tokenize with {} tokens", tokens.size());
        return tokens;
    }

}
