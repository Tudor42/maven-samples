package org.example.exceptions;

public class InvalidExpression extends Exception {
    public InvalidExpression(String message) {
        super(message);
    }
}
