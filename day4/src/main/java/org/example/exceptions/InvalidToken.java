package org.example.exceptions;

public class InvalidToken extends Exception {
    public InvalidToken(String message) {
        super(message);
    }
}
