package org.example.tokens;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.exp;
import static java.lang.Math.log;

public enum FunctionOperation implements Operation {
    SQRT("sqrt") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.size() != 1) {
                throw new ArithmeticException("Sqrt takes one argument");
            }
            if (operands.get(0).compareTo(new BigDecimal(0)) < 0) {
                throw new ArithmeticException("No square root of negative numbers");
            }
            return operands.get(0).sqrt(new MathContext(32, RoundingMode.HALF_UP));
        }
    },
    ADD("add") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            Optional<BigDecimal> res = operands.stream().reduce(BigDecimal::add);
            if (res.isPresent()) {
                return res.get();
            }
            throw new ArithmeticException("Add takes at least one argument");
        }
    },
    EXPONENT("exp") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.size() != 1) {
                throw new ArithmeticException("exp has only one argument");
            }
            return BigDecimal.valueOf(exp(operands.get(0).doubleValue()));
        }
    },
    LOGARITHM("log") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.size() != 1) {
                throw new ArithmeticException("log takes two arguments");
            }
            if (operands.get(0).doubleValue() <= 0) {
                throw new ArithmeticException("log has a positive domain");
            }
            return BigDecimal.valueOf(log(operands.get(0).doubleValue()));
        }
    },
    MAX("max") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.isEmpty()) {
                throw new ArithmeticException("Max missing operands");
            }
            return operands.stream().reduce(BigDecimal::max).get();
        }
    },
    MIN("min") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.isEmpty()) {
                throw new ArithmeticException("Min has to have at least one operand");
            }
            return operands.stream().reduce(BigDecimal::min).get();
        }
    },
    ROUND("round") {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.size() != 1) {
                throw new ArithmeticException("round takes one argument");
            }
            return operands.get(0).setScale(0, RoundingMode.HALF_UP);
        }
    };
    private final String symbol;

    FunctionOperation(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
