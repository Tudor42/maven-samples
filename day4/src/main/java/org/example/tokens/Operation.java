package org.example.tokens;

import java.math.BigDecimal;
import java.util.List;

public interface Operation {
    BigDecimal calc(List<BigDecimal> operands) throws ArithmeticException;
}
