package org.example.tokens;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.lang.Math.exp;
import static java.lang.Math.log;

public enum InplaceOperation implements Operation {
    ADD("\\+", 3) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            return operands.get(0).add(operands.get(1));
        }
    },
    SUBTRACT("-", 3) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            return operands.get(0).subtract(operands.get(1));
        }
    },
    DIVIDE("/", 2) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.get(1).doubleValue() == 0) {
                throw new ArithmeticException("Division by 0");
            }
            return operands.get(0).divide(operands.get(1), 15, RoundingMode.HALF_UP);
        }
    },
    MULTIPLY("\\*", 2) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            return operands.get(0).multiply(operands.get(1));
        }
    },
    MOD("%", 2) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands){
            if (operands.stream()
                    .filter(bd -> bd.signum() == 0 || bd.scale() <= 0 || bd.stripTrailingZeros().scale() <= 0)
                    .count() < 2) {
                throw new ArithmeticException("Modulus works only with integers");
            }
            return operands.get(0).remainder(operands.get(1)).setScale(0, RoundingMode.HALF_UP);
        }
    },
    POW("\\^", 1) {
        @Override
        public BigDecimal calc(List<BigDecimal> operands) {
            if (operands.get(1).signum() == 0 || operands.get(1).scale() <= 0
                    || operands.get(1).stripTrailingZeros().scale() <= 0) {
                return operands.get(0).pow(operands.get(1).intValue());
            }

            if (operands.get(0).doubleValue() >= 0) {
                return BigDecimal.valueOf(exp(operands.get(1).doubleValue() * log(operands.get(0).doubleValue())));
            }
            throw new ArithmeticException("Base cant be negative for real pow");
        }
    };

    private final String symbol;
    @Getter
    private final int precedence;

    InplaceOperation(String symbol, int precedence) {
        this.symbol = symbol;
        this.precedence = precedence;
    }

    @Override
    public String toString() {
        return symbol;
    }

}
