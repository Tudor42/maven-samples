package org.example.tokens;

public enum SpecialToken {
    NUMBER("[0-9]+[.]?[0-9]*"),
    LPARENTHESIS("\\("),
    RPARENTHESIS("\\)"),
    COMMA(","),
    END_TOKEN(null);

    private final String regex;

    SpecialToken(String regex) {
        this.regex = regex;
    }

    @Override
    public String toString() {
        return regex;
    }
}
