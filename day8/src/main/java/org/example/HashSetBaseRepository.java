package org.example;

import java.util.HashSet;
import java.util.Set;

public class HashSetBaseRepository<T> implements InMemoryRepository<T> {
    Set<T> set = new HashSet<>();

    public HashSetBaseRepository() {
    }

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }

    @Override
    public int size() {
        return set.size();
    }
}
