package org.example;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBaseRepository<T> implements InMemoryRepository<T> {
    List<T> list;

    public ArrayListBaseRepository() {
        list = new ArrayList<>();
    }

    @Override
    public void add(T e) {
        list.add(e);
    }

    @Override
    public boolean contains(T e) {
        return list.contains(e);
    }

    @Override
    public void remove(T e) {
        list.remove(e);
    }

    @Override
    public int size() {
        return list.size();
    }


}
