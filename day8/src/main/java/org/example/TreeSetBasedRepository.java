package org.example;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements InMemoryRepository<T> {
    Set<T> set = new TreeSet<>();

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }

    @Override
    public int size() {
        return set.size();
    }
}
