/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.jenkov;

import org.example.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 200, time = 2, timeUnit = TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
public class MyBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        public InMemoryRepository<Order> repository;
        public static final Random random = new Random(4567);

        public int lastUpdate;

        public static final int nr_elems = 100000;

        public enum TestCase{
            ARRAY_LIST, HASH_SET, TREE_SET
        }
        @Param
        private static TestCase testClass;

        @Setup(Level.Trial)
        public void setRepository() {
            if (testClass == TestCase.ARRAY_LIST) {
                repository = new ArrayListBaseRepository<>();
            }
            if (testClass == TestCase.HASH_SET) {
                repository = new HashSetBaseRepository<>();
            }
            if(testClass == TestCase.TREE_SET){
                repository = new TreeSetBasedRepository<>();
            }
            for (int i = 0; i < nr_elems; ++i) {
                repository.add(new Order(random.nextInt(nr_elems), 0, 0));
            }
        }

        @Setup(Level.Iteration)
        public void setLastUpdate(){
            lastUpdate = BenchmarkState.random.nextInt(BenchmarkState.nr_elems);
        }

        @TearDown(Level.Invocation)
        public void makeRepoDefaultSize(){
            if(lastUpdate == -1){
                return;
            }
            if(nr_elems < repository.size())
                repository.remove(new Order(lastUpdate, 0, 0));
            else
                repository.add(new Order(lastUpdate, 0, 0));
        }
    }


    @Benchmark
    public void testAdd(final BenchmarkState state) {
        state.repository.add(new Order(state.lastUpdate, 0, 0));
    }

    @Benchmark
    public void testContains(final BenchmarkState state) {
        state.repository.contains(new Order(BenchmarkState.random.nextInt(BenchmarkState.nr_elems), 0, 0));
    }

    @Benchmark
    public void testRemove(final BenchmarkState state) {
        state.repository.remove(new Order(state.lastUpdate, 0, 0));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .warmupIterations(0)
                .measurementIterations(5)
                .forks(1)  //a new java process for each trial
                .build();

        new Runner(opt).run();
    }
}

