package com.jenkov;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Measurement(iterations = 10, time = 200, timeUnit = MILLISECONDS)
@Warmup(iterations = 5, time = 50, timeUnit = MILLISECONDS)
@State(Scope.Benchmark)
public class StreamBenchmark {

    @Param({"100", "1000", "10000", "1000000", "50000000"})
    int nrElems;


    List<Integer> result = new ArrayList<>();

    @Setup
    public void setup() {
        for (int i = 1; i < nrElems; i++) {
            result.add(i);
        }
    }

    @Benchmark
    public void testForPrimitive() {
        int max = 0;
        for (int i = 1; i <= nrElems; i++) {
            int aux = i + 1;
            max = max < aux ? aux : max;
        }
    }

    @Benchmark
    public void testForInt() {
        int max = 0;
        for (int i = 1; i <= nrElems; i++) {
            Integer aux = i + 1;
            max = max < aux ? aux : max;
        }

    }

    @Benchmark
    public void testRangeClosed() {
        IntStream.rangeClosed(1, nrElems)
                .map(i -> i + 1)
                .max();
    }

    @Benchmark
    public void testRangeClosedUnordered() {
        IntStream.rangeClosed(1, nrElems)
                .unordered()
                .map(i -> i + 1)
                .max();
    }

    @Benchmark
    public void testListOf() {
        result.stream()
                .map(i -> i + 1)
                .max(Integer::compareTo);
    }

    @Benchmark
    public void testRangeClosedParallel() {
        IntStream.rangeClosed(1, nrElems)
                .unordered()
                .parallel()
                .map(i -> i + 1)
                .max();
    }

    @Benchmark
    public void testListOfParallel() {
        result.stream()
                .unordered()
                .parallel()
                .map(i -> i + 1)
                .max(Integer::compareTo);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StreamBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .result("benchmark-result/" + System.currentTimeMillis() + ".json")
                .forks(1)  //a new java process for each trial
                .build();

        new Runner(opt).run();
    }
}
