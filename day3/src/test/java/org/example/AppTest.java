package org.example;


import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThrows;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    App app;

    @Before
    public void setup(){
        app = new App();
    }

    @Test
    public void addNormalFlow(){
        assertEquals(app.add(10, 20), 30);
        assertEquals(app.add(0, 11), 11);
    }

    @Test
    public void addNegativeInput(){
        String expectedMessage = "Negative numbers not allowed";
        Exception e = assertThrows(IllegalArgumentException.class,
                ()->app.add(-1, 0));
        assertEquals(expectedMessage, e.getMessage());
        e = assertThrows(IllegalArgumentException.class, ()->app.add(0, -1));
        assertEquals(expectedMessage, e.getMessage());
    }

    @Test
    public void addMaxValueParams(){
        String expectedMessage = "max value for params: Integer.MAX_VALUE / 2 exceeded";
        Exception e = assertThrows(IllegalArgumentException.class, ()->app.add(Integer.MAX_VALUE, 0));
        assertEquals(expectedMessage, e.getMessage());
        e = assertThrows(IllegalArgumentException.class, ()->app.add(0, Integer.MAX_VALUE/2 + 1));
        assertEquals(expectedMessage, e.getMessage());
    }
}
