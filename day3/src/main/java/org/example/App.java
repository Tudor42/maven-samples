package org.example;


public class App 
{
    int add(int x, int y) throws IllegalArgumentException{
        if(x < 0 || y < 0) throw new IllegalArgumentException("Negative numbers not allowed");
        if(x > Integer.MAX_VALUE/2 || y > Integer.MAX_VALUE/2)
            throw new IllegalArgumentException("max value for params: Integer.MAX_VALUE / 2 exceeded");
        return x + y;
    }
}
